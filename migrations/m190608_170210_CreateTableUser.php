<?php

use yii\db\Migration;

/**
 * Class m190608_170210_CreateTableUser
 */
class m190608_170210_CreateTableUser extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user',

                [
                    'id' => $this->primaryKey(),
                    'login' => $this->string('255')->notNull()->unique(),
                    'dateCreated' => $this->dateTime(),
                    'dateUpdated' => $this->dateTime(),
                    'token' => $this->string(1024)->notNull()
                ]

            );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
