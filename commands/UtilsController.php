<?php


namespace app\commands;


use app\models\User;
use yii\console\Controller;

class UtilsController extends Controller
{

    /**
     * Создание тестового пользователя
     * @param $name
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreateUser($name)
    {
        $user = User::findOne(['login' => $name]);
        if (!$user) {
            $user = new User();
            $user->login = $name;
            $user->dateCreated = date('Y-m-d H:i:s', time());
            $user->token = md5($name);
            $user->save();
        } else {
            $user->login = $name;
            $user->dateUpdated = date('Y-m-d H:i:s', time());
            $user->token = md5($name);
            $user->update();
        }

        $result = [
            'Имя пользователя' => $name,
            'токен' => $user->token
        ];

        print_r($result);
    }

}
