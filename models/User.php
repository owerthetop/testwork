<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Class User
 * @property string $login
 * @property string $dateCreated
 * @property string $dateUpdated
 * @property string $token
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface
{
    public function getAuthKey()
    {
    }

    public function getId()
    {
    }

    public function validateAuthKey($authKey)
    {
    }

    public static function findIdentity($id)
    {
    }

    public static function tableName()
    {
        return 'user';
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token]);
    }
}
